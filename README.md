# 04.03.Taller.Tesis.2


* Taller de Tesis II - 18 a 22 hs.
* https://exactas-uba.zoom.us/j/85177721910
* ID de reunión: 851 7772 1910
* Código de acceso: tt22-mepc

La primera clase será el Martes 9 de Agosto. Luego coordinarán con los docentes las siguientes reuniones.


## Status

* There are two samples:
  * Api - Version 1.1 -> 1.9 M records (plus some extra files)
    * I should sample those tweets
    * From '2022-07-20', every day, at least 10 K days. Different hour of the day. 
  * Api - Version 2.0 -> 60 K per day, from 
    * From '2022-09-22', at least 40 K per day, most of them should have 60K.
    * (it should be 2.8 M -> 60.000 * 47 files )
    * It is not preprocessed. It has less columns (it has not the TW that has been RT)
* Previous steps
  * I present for 'taller de tesis I' (thesis workshop) - one assigment. Seed idea, and exploration of the pipeline
  * The scope was simpler
    * Capture tweets
    * Explore
    * Preproced
    * Bag of words + CountVectorizer 
      * Convert a collection of text documents to a matrix of token counts. 
    * PCA 
    * Kmeans
  * My idea was to explore the field. Know if it was possible. 
  * Use methods to capture tweets, explore the resulting JSON, and try some methods.   
  * Start reading some literature
  * Waiting for an answer about the assigment
* Next steps:
  * Create the graph of interelations
  * Explode that second sample
  * Find the hypotesis
  * Pandas is not parsing some files ()
  * The week after election is not over (Probably I should end the capture)


## Mail previo al taller


¿Tenés que cursar Taller de Tesis 2?

Ahora que aprobaste Taller de Tesis 1 (TT1) estarás considerando cursar Taller 2 en el segundo cuatrimestre. Una de las cosas que habrás notado es que TT2 no tiene horario fijo, aunque es una de las dos únicas materias obligatorias del segundo año ¿Por qué es esto?

Vamos por pasos. Tener los créditos por materias optativas y los Talleres de Tesis aprobados son requisitos para completar segundo año y poder defender la tesis. El plan de tesis se puede presentar en cualquier momento del segundo año, solo hace falta tener aprobadas las materias de primer año. Ahora bien, ¿es frecuente presentar el plan de tesis durante el segundo año? No, para nada. La razón es que pensar un tema de tesis, buscar alguien que dirija la tesis, leer la literatura, preparar y redactar el proyecto es un esfuerzo importante. Y es difícil hacerlo mientras además estás cursando los talleres y las optativas.

Mientras que TT1 está más orientado a cerrar los trabajos de especialización, en TT2 buscamos ayudarte a llevar adelante tu tesis de maestría. Como corresponde a un taller, hay poco contenido y bastantes consultas e interacciones con los docentes. Entonces volvamos al título de este mensaje ¿Es este el momento de cursar TT2? Lo recomendable es que curses TT2 cuando ya hayas decidido encarar la tesis. No hace falta que tengas decidido el tema ni entrevistado posibles directores o directoras, te podemos ayudar con eso. Lo importante es que tengas el tiempo suficiente y la decisión de hacerlo. Y en general esto ocurre cuando ya terminaste de cursar.

Si no cursás TT2 este cuatrimestre, no hay problema hacerlo el año que viene, porque la regularidad en el posgrado la mantenés durante varios años. Además. Si hay suficientes interesados, podemos dictar TT2 también en el primer cuatrimestre. Incluso, si ya cursaste y aprobaste el taller y por el motivo que sea recién ahora estás iniciando la tesis, podés volver a cursar (sin prender la aprobación previa). Para este caso especial, comunicate con las secretarias.

Finalmente, queda una pregunta que sabemos que siempre aparece a esta altura de la cursada: ¿Vale la pena hacer la tesis de maestría? También sabemos que una fracción nada despreciable piensa que no. Resulta que hay unos cuántos motivos que justifican el esfuerzo. Y que si bien es común pensar que lo importante es adquirir conocimientos, plantearte el desafío de generarlos vos o trabajar en la resolución de un problema complejo, hace una diferencia importante. Si querés saber más sobre esto podés leer este post que escribió hace un tiempo uno de nosotros (http://datamining.dc.uba.ar/predictivos/?p=671).

Si tenés dudas y todavía no tomaste una decisión, podés sumarte a la primera reunión del curso.
Saludos,

Los profesores de Taller de Tesis 2.
María Elena Buemi
Marcelo Soria
