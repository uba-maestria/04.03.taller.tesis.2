# Jupyter lab con keras y tensorflow

Imagen util para la clase de redes neuronales, de la maestría

Imagen base: Tensorflow oficial ()

Items agregados:
* Jupyter lab 
* Black linter  
	* Node/npm para instalar el linter (https://stackoverflow.com/questions/36399848/install-node-in-dockerfile)
* Working directory: '/home/jovyan/work'
	* Vinculado en el compose con la carpeta: 'work'

## Items del repositorio

* ./Jupyterlab
	* Dockerfile
	* Requeriments de la imagen base
* ./work
	* Carpeta para notebooks
* ./docker-compose.yml
	* Docker compose, muy simple
	* Build con la imagen que está en la carpeta jupyterlab
	* Puerto compartido: 28888 (Relacionado al puerto 8888 de jupyterlab)
		* Para ingresar: http://0.0.0.0:28888/lab

## Instrucciones

Builder imagen embebida en el compose

	docker-compose build 

Levantar el docker compose

	docker-compose up -d --no-deps

Para ingresar, en el navegador: http://0.0.0.0:28888/lab


## Requerimientos

* Docker
* Docker compose

## Referencias


* tensorflow official docker image: https://hub.docker.com/r/tensorflow/tensorflow/
* tensorflow official docker flavours: https://www.tensorflow.org/install/docker
* jupyter lab: https://jupyterlab.readthedocs.io/en/stable/getting_started/starting.html
* npm in docker: https://stackoverflow.com/questions/36399848/install-node-in-dockerfile
* Someone making docker tensorflow keras jupyer: https://hands-on.cloud/how-to-run-jupiter-keras-tensorflow-pandas-sklearn-and-matplotlib-in-docker-container/#dockerfile
* Linter: https://jupyterlab-code-formatter.readthedocs.io/en/latest/installation.html