# Consumo de noticias en twitter

Presentación que hicieron en 2021 en la última clase de Data mining en Ciencia y Tecnología.

* Autor: Tomás Cicchini <tomas.cicchini@gmail.com> 
* Director: Pablo Balenzuela <balen@df.uba.ar>.

Adjunto también las slides, me parece que está buenisimo como tema de investigación para la tesis. Te sugiero que le escribas a Tomás y Pablo y si no va por ahí, tengo en mente un par más de personas que pueden estar interesadas en dirigir una tesis en el tema.

(Charlado con Juan)